from environs import Env

env = Env()
env.read_env()

POSTGRES_USER = env.str("POSTGRES_USER")
POSTGRES_PASSWORD = env.str("POSTGRES_PASSWORD")
POSTGRES_DB = env.str("POSTGRES_DB")
POSTGRES_HOST = env.str("POSTGRES_HOST")
DOMAIN = env.str("DOMAIN", "")
if not DOMAIN:
    POSTGRES_HOST="localhost"

VERSION = env.str("IMAGE_TAG", "")

DATABASE_URL = env.str("DATABASE_URL", f"postgresql+psycopg2://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}/{POSTGRES_DB}")