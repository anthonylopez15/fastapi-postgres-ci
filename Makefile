delete:
	kubectl delete deployment fastapi-deployment
	kubectl delete service fastapi-service
	kubectl delete deployment postgres
	kubectl delete service postgres-service
	kubectl delete configMap postgres-config
	kubectl delete persistentvolumeclaim postgres-pv-claim
	kubectl delete persistentvolume postgres-pv-volume
	kubectl delete secret postgres-secrets

apply:
	kubectl apply -f k8s/postgres-secret.yaml
	kubectl create -f k8s/my-namespace.yaml
	kubectl apply -f k8s/postgres.yaml
	kubectl apply -f k8s/fastapi.yaml

delete_fastapi:
	kubectl delete deployment fastapi-deployment
	kubectl delete service fastapi-service
	kubectl delete deployment postgres
	kubectl delete service postgres-service
